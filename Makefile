bindir ?= $(PWD)

open_wrapper.sh: open_wrapper.sh.in
	sed -e 's#@bindir@#$(bindir)#g' $< > $@ \
	  && chmod +x $@

clean:
	rm -f open_wrapper.sh
